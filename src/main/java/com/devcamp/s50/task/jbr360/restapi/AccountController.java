package com.devcamp.s50.task.jbr360.restapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
@CrossOrigin
public class AccountController {
    @GetMapping("/accounts")

    public ArrayList<Account> getListAccount() {
        Account account1 = new Account("01", "huy", 12000);
        Account account2 = new Account("02", "hoang", 14000);
        Account account3 = new Account("03", "lan", 13000);

        System.out.println(account1);
        System.out.println(account2);
        System.out.println(account3);
        ArrayList<Account> accounts = new ArrayList<>();
        accounts.add(account1);
        accounts.add(account2);
        accounts.add(account3);
        return accounts;
    }
}
